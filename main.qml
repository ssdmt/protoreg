import QtQuick 2.7
import QtQuick.Window 2.2



Window {
    visible: true
    width: 320
    height: 240
    title: qsTr("Prototype D6 320x240")

    Image {
        anchors.fill: parent
        source: "qrc:///img/flowers_bg.png"
        visible: true
        opacity: 0.3
    }

    Column {
        anchors.fill: parent
        ItemMenu {
            text: "ОСНОВНОЕ"
        }
        ItemMenu {
            text: "ДИСПЛЕИ"
        }
        ItemMenu {
            text: "ИЗОБРАЖЕНИЕ"
        }
        ItemMenu {
            text: "РЕЖИМ ПАРКОВКИ"
        }
        ItemMenu {
            text: "СПИДКАМ"
        }
    }
}
