import QtQuick 2.7
import QtGraphicalEffects 1.0

Item {
    property var visibleMarker: false
    property var text

    width: parent.width
    height: 48

    Rectangle {
        id: rectMarker
        anchors.fill: parent
        visible: visibleMarker
        color: "#003f88"
        opacity: .4
        radius: 3
        border.width: 2
        border.color: "#0074cc"
        z:1
    }

    Text {
        anchors.fill: parent
        visible: true
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        color: "#ffffff"
        style: Text.Outline
        styleColor: "#3358a2"
        font.family: "Tahoma"
        font.pointSize: 18
        text: parent.text
        z:2
    }

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onEntered: parent.visibleMarker = true
        onExited: parent.visibleMarker = false
    }

//    FastBlur {
//        anchors.fill: rectMarker
//        source: rectMarker
//        visible: visibleMarker
//        radius: 64
//    }
}






















//Text {
//    property var visibleMarker: false
//    width: parent.width
//    horizontalAlignment: Text.AlignHCenter
//    color: "#00cc00"
////    style: Text.Outline
////    styleColor: "#009900"
//    font.pointSize: 18

//    Rectangle {
//        id: rectMarker
//        visible: visibleMarker
//        anchors.fill: parent

////        color: "transparent"
//        color: "#330000"
//        radius: 3
//        border.width: 2
//        border.color: "#aa0000"
//    }

//    FastBlur {
//        anchors.fill: rectMarker
//        source: rectMarker

//        visible: visibleMarker
//        radius: 8
//    }
//}

